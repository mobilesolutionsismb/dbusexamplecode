#include "dbusobjectsender.h"
#include <QDebug>

DBusObjectSender::DBusObjectSender(QObject *parent) : QObject(parent)
{
    setTimerEvent();
}

void DBusObjectSender::setTimerEvent()
{
    timerEvent.stop();
    timerEvent.setSingleShot(true);
    timerEvent.setInterval(10);

    connect(&timerEvent,SIGNAL(timeout()),this,SLOT(onElapsedTimer()));

    //connect(this,SIGNAL(requestPerformAsyncAction()),this,SLOT(onLocalSlotAssociatedToRemoteSignal()));
}

QString DBusObjectSender::DBUS_METHOD_NAME(const QString &arg)
{
    //QMetaObject::invokeMethod(QCoreApplication::instance(), "quit");

    #if 1
//    timerEvent.start();
    #else
    emit DBUS_SIGNAL_NAME("ResultDBUSServer");
    #endif

    return QString("performAction(\"%1\") got called").arg(arg);
}

Q_SCRIPTABLE CustomClass DBusObjectSender::getCustomClass(QString string, int paramInt)
{
    return CustomClass(string,paramInt);
}

QString DBusObjectSender::DBUS_ASYNCMETHOD_NAME(const QString& reply)
{
    qDebug()<<"Reply Event Name: "<<reply;

    _queueStringReceived.append(reply);
    timerEvent.start();

    return "DBusObjectSender:"+reply;
}

void DBusObjectSender::onElapsedTimer()
{
    timerEvent.stop();

    if(_queueStringReceived.isEmpty()==true)
        return;

    QString element;

    element = _queueStringReceived.dequeue();

    qDebug()<<"Elapsed Timer. Emit SIGNAL, Received: "<<element;

    emit DBUS_SIGNAL_NAME(element.toLower());

    if(_queueStringReceived.isEmpty()==false)
        timerEvent.start();
}

void DBusObjectSender::onLocalSlotAssociatedToRemoteSignal()
{
    qDebug()<<"Emit LocalSlot, ReplySignal NO INPUT";
}
