#include <QCoreApplication>
#include <QDebug>
#include "dbusservice.h"
#include "commondefines.h"
#include "applicationinfo.h"
#include "customclass.h"

static DBusService dbusService(DBUS_SERVICE_NAME,DBUS_SERVICE_PATH, DBUS_SERVICE_INTERFACE);

int main(int argc, char *argv[])
{
    QCoreApplication application(argc, argv);

    ApplicationInfo::printApplicationInfo("ServiceGenerator");

    CustomClass::registerMetaType();

    if(!dbusService.configureAndLaunchService())
    {
        qWarning()<< "Unable To Launch DBUS Service";
        return -1;
    }

    qInfo()<< "DBUS Service Listening: "<<QString(DBUS_SERVICE_NAME);

    return application.exec();
}
