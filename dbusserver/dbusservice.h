#ifndef DBUSSERVICE_H
#define DBUSSERVICE_H

#include <QObject>
#include <QtDBus/QtDBus>
#include "commondefines.h"
#include "dbusobjectsender.h"

class DBusService : public QObject
{
    Q_OBJECT

private:
    #if 0
    QDBusServiceWatcher serviceWatcher;
    #endif

    QString _service;
    QString _path;
    QString _interface;
    QDBusConnection _connection;
    QDBusInterface*  ptrinterfaceObject;
    QDBusServiceWatcher* ptrDBUSServiceWatcher;

    DBusObjectSender objectSender;
public:
    static const char* staticInterfaceName() { return DBUS_SERVICE_NAME; }

public:
    explicit DBusService(const QString &service, const QString &path, const QString &interface, const QDBusConnection &connection=QDBusConnection::DBUS_SELECTED_CONNECTION(), QObject *parent=nullptr);
    virtual ~DBusService() override;

    bool configureAndLaunchService();

signals:

public slots:
    void serviceDBUSRegistered(const QString& serviceName);

};

#endif // DBUSSERVICE_H
