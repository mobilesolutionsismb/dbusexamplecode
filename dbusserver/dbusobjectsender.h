#ifndef DBUSOBJECTSENDER_H
#define DBUSOBJECTSENDER_H

#include <QtCore/QObject>
#include <QDBusAbstractAdaptor>
#include <QTimer>
#include <QQueue>
#include "commondefines.h"
#include "customclass.h"

class DBusObjectSender : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", DBUS_SERVICE_INTERFACE);
    //Q_PROPERTY(QString value READ value WRITE setValue)
    private:
        QTimer          timerEvent;
        QQueue<QString> _queueStringReceived;

        void setTimerEvent();
    public:
        explicit DBusObjectSender(QObject *parent = nullptr);

    signals:
        Q_SCRIPTABLE void DBUS_SIGNAL_NAME(const QString& param);

    public slots:
        Q_SCRIPTABLE QString DBUS_METHOD_NAME(const QString &arg);
        Q_SCRIPTABLE QString DBUS_ASYNCMETHOD_NAME(const QString& reply);
        Q_SCRIPTABLE CustomClass getCustomClass(QString string, int paramInt);

        void onLocalSlotAssociatedToRemoteSignal();
        void onElapsedTimer();
};

#endif // DBUSOBJECTSENDER_H
