#include "dbusservice.h"
#include <QDebug>

DBusService::DBusService(const QString &service, const QString &path, const QString& interface , const QDBusConnection &connection, QObject *parent) : QObject(parent)
  , _service(service)
  , _path(path)
  , _interface(interface)
  , _connection(connection)
  , ptrinterfaceObject(nullptr)
    //QDBusAbstractInterface(service, path, staticInterfaceName(), connection, parent)
{
    #if 0
    serviceWatcher.addWatchedService(service);
    serviceWatcher.setWatchMode(QDBusServiceWatcher::WatchModeFlag::WatchForRegistration);
    serviceWatcher.setConnection(connection);
    #endif
}

void DBusService::serviceDBUSRegistered(const QString& serviceName)
{
    qDebug()<<"serviceDBUSRegistered Raised, servicename: "<<serviceName;
}

bool DBusService::configureAndLaunchService()
{
    qDebug()<<"Try Launch DBUS Service Provider";
    if (!_connection.isConnected()) {
        fprintf(stderr, "Cannot connect to the D-Bus session bus.\n"
                "To start it, run:\n"
                "\teval `dbus-launch --auto-syntax`\n");
        return false;
    }

    qDebug()<<"DBUS Session Bus is Connected";

    if (!_connection.registerService(_service)) {
        fprintf(stderr, "%s\n",
                qPrintable(_connection.lastError().message()));

        return false;
    }

    qDebug()<<"DBUS Registration Service Correctly performed: "<<_service;

    ptrinterfaceObject = new QDBusInterface(_service,
                                            _path,
                                            _interface,
                                            _connection,
                                            &objectSender);

    if(ptrinterfaceObject->isValid()==false)
    {
        qWarning()<<"DBUS QDBusInterface Init Failed: "<<ptrinterfaceObject->lastError().message();

        return false;
    }

    qDebug()<<"DBUS QDBusInterface Success";

    if(!_connection.registerObject(_path,
                                                     _interface,
                                                     &objectSender,
                                                     QDBusConnection::ExportScriptableSlots|QDBusConnection::ExportScriptableSignals)){
        fprintf(stderr, "%s\n",
                qPrintable(_connection.lastError().message()));

        return false;
    }

    qDebug()<<"DBUS Registration Object Correctly performed";

    return true;
}

DBusService::~DBusService()
{

}

