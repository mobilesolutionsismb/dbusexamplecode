#ifndef CUSTOMCLASS_DERIVED_H
#define CUSTOMCLASS_DERIVED_H

#include <QObject>
#include <QtDBus>

class CustomClassDerived: public QObject
{
    Q_OBJECT
    public:
        void copy(const CustomClassDerived& c1);

    public:
        CustomClassDerived(QObject* parent=nullptr);
        CustomClassDerived(const CustomClassDerived& c1);
        CustomClassDerived(const QString& testString, int parameterInteger);

        friend QDBusArgument &operator<<(QDBusArgument &argument, const CustomClassDerived &message);
        friend const QDBusArgument &operator>>(const QDBusArgument &argument, CustomClassDerived &message);

        static void registerMetaType();

        QString testString;
        int     parameterInteger;

        CustomClassDerived& operator=(const CustomClassDerived& c);

        QString toString();
};

Q_DECLARE_METATYPE(CustomClassDerived)

#endif // CUSTOMCLASS_H
