SOLUTION_BASE_FOLDER=$$clean_path($${PWD}/..)
SOLUTION_BUILD_DIR=$$SOLUTION_BASE_FOLDER/build

CONFIG += $$(KITSDKNAME)

IMX8MMYoctoSumo {
    DEFINES += YOCTO_PROJECT_BUILD=1
}

!IMX8MMYoctoSumo {
    DEFINES += YOCTO_PROJECT_BUILD=-1
}

CONFIG(debug, debug|release) {
    #CONFIG += testing
    PROJECT_BUILD_DIR=$$SOLUTION_BUILD_DIR/$$(KITSDKNAME)/$$TARGET/debug

    message(Build Directory: $$PROJECT_BUILD_DIR)
}

CONFIG(release, debug|release) {
    #CONFIG += testing
    PROJECT_BUILD_DIR=$$SOLUTION_BUILD_DIR/$$(KITSDKNAME)/$$TARGET/release

    message(Build Directory: $$PROJECT_BUILD_DIR)
}

PATH_COMMONSOURCE=$$SOLUTION_BASE_FOLDER/common

INCLUDEPATH += $$PATH_COMMONSOURCE

MOC_DIR = $$PROJECT_BUILD_DIR/moc
UI_DIR = $$PROJECT_BUILD_DIR/intermediate/ui
RCC_DIR = $$PROJECT_BUILD_DIR/intermediate/rcc
OBJECTS_DIR = $$PROJECT_BUILD_DIR/intermediate/obj

DESTDIR = $$PROJECT_BUILD_DIR

HEADERS += \
    $$PATH_COMMONSOURCE/applicationinfo.h \
    $$PATH_COMMONSOURCE/commondefines.h \
    $$PATH_COMMONSOURCE/customclass.h \
    $$PATH_COMMONSOURCE/customclassderived.h

SOURCES += \
    $$PATH_COMMONSOURCE/applicationinfo.cpp \
    $$PATH_COMMONSOURCE/customclass.cpp \
    $$PATH_COMMONSOURCE/customclassderived.cpp


