#ifndef COMMONDEFINES_H
#define COMMONDEFINES_H

#define FB(arg) #arg
#define FC(arg) FB(#arg)

#define DBUS_SERVICE_NAME   "org.freedesktop.toto"
#define DBUS_SERVICE_PATH   "/DBusSender"
#define DBUS_SERVICE_INTERFACE   DBUS_SERVICE_NAME ".interface"
#define DBUS_METHOD_NAME        performAction
#define DBUS_ASYNCMETHOD_NAME   performAsyncAction
#define DBUS_SIGNAL_NAME        requestPerformAsyncAction

#if defined(YOCTO_PROJECT_BUILD) && (YOCTO_PROJECT_BUILD>0)
#define DBUS_SELECTED_CONNECTION systemBus
#else
#define DBUS_SELECTED_CONNECTION sessionBus
#endif

#define DBUS_METHOD_STRING          "performAction" //FC(DBUS_METHOD_NAME)
#define DBUS_ASYNC_METHOD_NAME      "performAsyncAction"
#define DBUS_SIGNAL_STRING          "requestPerformAsyncAction"

#endif // COMMONDEFINES_H
