#include "customclassderived.h"

CustomClassDerived::CustomClassDerived(QObject *parent): QObject(parent)
{
}

CustomClassDerived::CustomClassDerived(const CustomClassDerived& c1): QObject(nullptr)
{
    copy(c1);
}

CustomClassDerived::CustomClassDerived(const QString& testString, int parameterInteger)
{
    this->testString        = testString.toUpper();
    this->parameterInteger  = parameterInteger+2;
}

void CustomClassDerived::copy(const CustomClassDerived& c1)
{
    this->testString        = c1.testString;
    this->parameterInteger  = c1.parameterInteger;
}

CustomClassDerived& CustomClassDerived::operator=(const CustomClassDerived& c)
{
    copy(c);

    return *this;
}

QString CustomClassDerived::toString()
{
    return  QString("testStringDERIVED: ")+testString+
            QString(";")+
            QString("paramIntegerDERIVED: ")+QString::number(parameterInteger);
}

void CustomClassDerived::registerMetaType()
{
    qRegisterMetaType<CustomClassDerived>("CustomClassDerived");

    qDBusRegisterMetaType<CustomClassDerived>();
}

QDBusArgument &operator<<(QDBusArgument &argument, const CustomClassDerived& message)
{
    argument.beginStructure();
    argument << message.testString;
    argument << message.parameterInteger;
    argument.endStructure();

    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, CustomClassDerived &message)
{
    argument.beginStructure();
    argument >> message.testString;
    argument >> message.parameterInteger;
    argument.endStructure();

    return argument;
}

