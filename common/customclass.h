#ifndef CUSTOMCLASS_H
#define CUSTOMCLASS_H

#include <QObject>
#include <QtDBus>
#include "customclassderived.h"

class CustomClass: public QObject
{
    Q_OBJECT
    private:
        void copy(const CustomClass& c1);

    public:
        CustomClass(QObject* parent=nullptr);
        CustomClass(const CustomClass& c1);
        CustomClass(const QString& testString, int parameterInteger);

        friend QDBusArgument &operator<<(QDBusArgument &argument, const CustomClass &message);
        friend const QDBusArgument &operator>>(const QDBusArgument &argument, CustomClass &message);

        static void registerMetaType();

        QString testString;
        int     parameterInteger;
        CustomClassDerived ccd;

        CustomClass& operator=(const CustomClass& c);

        QString toString();
};

Q_DECLARE_METATYPE(CustomClass)

#endif // CUSTOMCLASS_H
