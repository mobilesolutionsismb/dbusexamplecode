#ifndef APPLICATIONINFO_H
#define APPLICATIONINFO_H

#include <QString>

namespace ApplicationInfo
{
    void printApplicationInfo(const QString& applicationName);
}

#endif // APPLICATIONINFO_H
