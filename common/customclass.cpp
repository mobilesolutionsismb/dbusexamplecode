#include "customclass.h"

CustomClass::CustomClass(QObject *parent): QObject(parent)
{
}

CustomClass::CustomClass(const CustomClass& c1): QObject(nullptr)
{
    copy(c1);
}

CustomClass::CustomClass(const QString& testString, int parameterInteger)
{
    this->testString        = testString.toLower();
    this->parameterInteger  = parameterInteger+1;
    this->ccd = CustomClassDerived(testString,parameterInteger);
}

void CustomClass::copy(const CustomClass& c1)
{
    this->testString        = c1.testString;
    this->parameterInteger  = c1.parameterInteger;
    this->ccd.copy(c1.ccd);
}

CustomClass& CustomClass::operator=(const CustomClass& c)
{
    copy(c);

    return *this;
}

QString CustomClass::toString()
{
    return  QString("testString: ")+testString+
            QString(";")+
            QString("paramInteger: ")+QString::number(parameterInteger)
            +QString(";")+
            ccd.toString();
}

void CustomClass::registerMetaType()
{
    qRegisterMetaType<CustomClass>("CustomClass");

    qDBusRegisterMetaType<CustomClass>();

    CustomClassDerived::registerMetaType();
}

QDBusArgument &operator<<(QDBusArgument &argument, const CustomClass& message)
{
    argument.beginStructure();
    argument << message.testString;
    argument << message.parameterInteger;
    argument << message.ccd;
    argument.endStructure();


    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, CustomClass &message)
{
    argument.beginStructure();
    argument >> message.testString;
    argument >> message.parameterInteger;
    argument >> message.ccd;
    argument.endStructure();

    return argument;
}

