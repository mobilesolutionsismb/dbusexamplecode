#include "applicationinfo.h"
#include "commondefines.h"
#include <QDebug>

void ApplicationInfo::printApplicationInfo(const QString& applicationName)
{
    qInfo() << "DBUS "<<applicationName<< " Starting, Version: "<<QString(__DATE__)<<"|"<<QString(__TIME__);

    #if defined(YOCTO_PROJECT_BUILD) && (YOCTO_PROJECT_BUILD>0)
    qInfo()<< "Build For Yocto Project (systemBus)";
    #else
    qInfo()<< "Build For Linux Desktop (sessionBus)";
    #endif

    qInfo()<< "DBUS_SERVICE_NAME: "<<QString(DBUS_SERVICE_NAME);
    qInfo()<< "DBUS_SERVICE_PATH: "<<QString(DBUS_SERVICE_PATH);
    qInfo()<< "DBUS_SERVICE_INTERFACE: "<<QString(DBUS_SERVICE_INTERFACE);
}
