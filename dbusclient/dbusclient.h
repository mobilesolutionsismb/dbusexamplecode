#ifndef DBUSCLIENT_H
#define DBUSCLIENT_H

#include <QObject>
#include <QtDBus/QtDBus>
#include <QTimer>
#include <QQueue>
#include "commondefines.h"

class DBusClient : public QObject
{
    Q_OBJECT

private:
    QTimer                      _timerDBusRequest;
    QQueue<QString>             _queueBuffer;

    QStringList                 _listStringToSend;
    int                         _elemToPick;
private:
    QDBusPendingCallWatcher*    _ptrWatcher;
    QDBusPendingCall*           _ptrAsync;
    QDBusInterface*             _ptrIface;
    QString                     _service;
    QString                     _path;
    QString                     _interface;
    QDBusConnection             _connection;

private:
    void configureTimer();
public:
    explicit DBusClient(const QString &service, const QString &path, const QString &interface = QString(),
                        const QDBusConnection &connection = QDBusConnection::DBUS_SELECTED_CONNECTION(),QObject *parent = nullptr);

    bool configureAndLaunch();
    void startTimer();

signals:

public slots:
    void callFinishedSlot(QDBusPendingCallWatcher *call);
    void callSlotFromDBus(const QString& result);
    void fsmTimerRequests();

};

#endif // DBUSCLIENT_H
