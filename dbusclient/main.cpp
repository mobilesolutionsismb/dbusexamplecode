#include <QCoreApplication>
#include "dbusclient.h"
#include "applicationinfo.h"
#include "customclass.h"

static DBusClient* ptrDbusClient = nullptr;

int main(int argc, char *argv[])
{
    QCoreApplication application(argc, argv);

    //Q_DECLARE_METATYPE(CustomClass)

    ApplicationInfo::printApplicationInfo("Client");

    CustomClass::registerMetaType();

    ptrDbusClient = new DBusClient(DBUS_SERVICE_NAME,DBUS_SERVICE_PATH,DBUS_SERVICE_INTERFACE);

    if(ptrDbusClient==nullptr)
        return -1;

    if(!ptrDbusClient->configureAndLaunch())
    {
        qWarning()<< "Unable To Launch DBUS Client";
        return -1;
    }

    ptrDbusClient->startTimer();

    qInfo()<< "DBUS Client Attach Service: "<<QString(DBUS_SERVICE_NAME)<<", Path: "<<QString(DBUS_SERVICE_PATH);

    return application.exec();
}
