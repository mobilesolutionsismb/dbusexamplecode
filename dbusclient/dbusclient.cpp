#include "dbusclient.h"
#include "commondefines.h"
#include "customclass.h"
#include <QDebug>
#include <QMetaType>

DBusClient::DBusClient(const QString &service, const QString &path, const QString &interface,
                       const QDBusConnection &connection,QObject *parent) : QObject(parent)
    ,   _elemToPick(0)
    ,   _ptrWatcher(nullptr)
    ,   _ptrAsync(nullptr)
    ,   _service(service)
    ,   _path(path)
    ,   _interface(interface)
    ,   _connection(connection)
{
    _listStringToSend.append("CIAO");
    _listStringToSend.append("NUOVAPAROLA");
    _listStringToSend.append("ALTRO");
    _listStringToSend.append("SECONDO");
    _listStringToSend.append("FORTEZZA");

    configureTimer();
}

void DBusClient::configureTimer()
{
    _timerDBusRequest.stop();
    _timerDBusRequest.setSingleShot(true);
    _timerDBusRequest.setInterval(2000);
    QObject::connect(&_timerDBusRequest,SIGNAL(timeout()),this, SLOT(fsmTimerRequests()));
}

bool DBusClient::configureAndLaunch()
{
    qDebug()<<"Trying Attach to Service: "<<_service;

    if (!_connection.isConnected()) {
        fprintf(stderr, "Cannot connect to the D-Bus session bus.\n"
                "To start it, run:\n"
                "\teval `dbus-launch --auto-syntax`\n");
        return false;
    }

    qDebug()<<"SessionBus Connected";

    _ptrIface = new QDBusInterface(_service, _path, _interface, _connection);
    if (_ptrIface->isValid()==false)
    {
        qDebug()<<"iface Not valid: "<<_path<<", Error: "<<_connection.lastError().message();
        return false;
    }

    qRegisterMetaType<CustomClass>("CustomClass");

    //bool flagConnect = connect(_ptrIface, SIGNAL(requestPerformAsyncAction(QString)),this,SLOT(callSlotFromDBus(QString)));
    bool flagConnect = _connection.connect(_service, _path, _interface, DBUS_SIGNAL_STRING, this, SLOT(callSlotFromDBus(QString)));
    qDebug()<<"Result SigSlot Connect Through DBUS: "<<(flagConnect==true?"Success":"Failed");

    QDBusReply<CustomClass> replyCustom = _ptrIface->call("getCustomClass", "MESSAGGIO TUTTO STAMPATELLO", 5);

    if(replyCustom.isValid()==false)
    {
        fprintf(stderr, "Call %s failed: %s\n","getCustomClass",
                qPrintable(replyCustom.error().message()));
        return false;
    }

    CustomClass c = replyCustom.value();

    printf("Reply CustomClass was: %s\n", qPrintable(c.toString()));

    QDBusReply<QString> reply = _ptrIface->call(DBUS_METHOD_STRING, "Message Sent");
    if (reply.isValid()==false)
    {
        fprintf(stderr, "Call %s failed: %s\n",DBUS_METHOD_STRING,
                qPrintable(reply.error().message()));
        return false;
    }
    printf("Reply was: %s\n", qPrintable(reply.value()));

    _ptrAsync   = new QDBusPendingCall(_ptrIface->asyncCall(DBUS_ASYNC_METHOD_NAME, "Message Sent"));
    _ptrWatcher = new QDBusPendingCallWatcher(*_ptrAsync, this);

    QObject::connect(_ptrWatcher,
                     SIGNAL(finished(QDBusPendingCallWatcher*)),
                     this,
                     SLOT(callFinishedSlot(QDBusPendingCallWatcher*)));
    return 1;
}

void DBusClient::startTimer()
{
    _timerDBusRequest.start();
}

void DBusClient::callSlotFromDBus(const QString &result)
{
    qInfo()<<"callSlotFromDBus Received: "<<result;
}

void DBusClient::fsmTimerRequests()
{
    _timerDBusRequest.stop();

    if(_ptrIface==nullptr)
        return;

    if(_elemToPick==_listStringToSend.length())
        _elemToPick = 0;

    qInfo()<<"Elapsed Timeout Timer, Trying request sending Word: "<<_listStringToSend.at(_elemToPick);

    _ptrIface->asyncCall(DBUS_ASYNC_METHOD_NAME, _listStringToSend.at(_elemToPick++));
    _timerDBusRequest.start();
}

void DBusClient::callFinishedSlot(QDBusPendingCallWatcher *call)
{
    QDBusPendingReply<QString> reply = *call;
    if (reply.isError()) {
        qCritical()<<"callFinishedSlot RECEIVED ERROR: "<<reply.error().message();
        return;
    } else {
        QString text = reply.argumentAt<0>();
        //QByteArray data = reply.argumentAt<1>();

        qInfo()<<"callFinishedSlot Received Async Answer: "<<text;
    }
    call->deleteLater();
}
