# How To Use It #

## Desktop ##

Add Environment variable KITSDKNAME to Build Environment and set to Desktop. Build Solution, then run DBusServer and DBusClient.

## Embedded (Yocto) ##

Add Environment variable KITSDKNAME to Build Environment and set to IMX8MMYoctoSumo. Copy file resources/org.freedesktop.toto.conf to /usr/share/dbus-1/system.d/ and restart dbus service using command: 

```console
systemctl restart dbus.
```
