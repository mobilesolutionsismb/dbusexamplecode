#include <QCoreApplication>
#include "dbuslistener.h"
#include <QDebug>
#include "applicationinfo.h"

static DBusListener dbusListener;


int main(int argc, char *argv[])
{
    QCoreApplication application(argc, argv);

    ApplicationInfo::printApplicationInfo("Listener");

    if(dbusListener.initializeAndLaunch()==false)
    {
        qCritical()<<"DBUS Listener Exit Failure!";

        return -1;
    }

    return application.exec();
}
