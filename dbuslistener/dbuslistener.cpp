#include "dbuslistener.h"
#include "commondefines.h"
#include <QDBusConnection>
#include <QtDBus/QtDBus>
#include <QDebug>

DBusListener::DBusListener(QObject *parent): QObject(parent)
{

}

bool DBusListener::initializeAndLaunch()
{
    bool flagRec;

    if(!QDBusConnection::DBUS_SELECTED_CONNECTION().isConnected())
    {
        qCritical() << "Cannot connect to the D-Bus session bus!";
        return false;
    }


    flagRec = QDBusConnection::DBUS_SELECTED_CONNECTION().connect(DBUS_SERVICE_NAME, DBUS_SERVICE_PATH, DBUS_SERVICE_INTERFACE, DBUS_SIGNAL_STRING,
                                          this, SLOT(receivedSlotFromDbus(QString)));

    qDebug()<<"Result SigSlot Connect Through DBUS: "<<(flagRec==true?"Success":"Failed");

    return flagRec;
}

void DBusListener::receivedSlotFromDbus(const QString& param)
{
    qInfo()<<"ReceivedSlotFromDBUS: "<<param;
}
