#ifndef DBUSLISTENER_H
#define DBUSLISTENER_H

#include <QObject>

class DBusListener: public QObject
{
        Q_OBJECT

    public:
        DBusListener(QObject* parent=nullptr);

        bool initializeAndLaunch();

    public slots:
        void receivedSlotFromDbus(const QString &param);
};

#endif // DBUSLISTENER_H
